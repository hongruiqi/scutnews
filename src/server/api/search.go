package main

import (
	"encoding/json"
	"fmt"
	"launchpad.net/mgo/bson"
	"log"
	"net/http"
	"strconv"
	"strings"
)

var _ = fmt.Println
var _ = log.Fatal

func init() {
	http.HandleFunc("/api/search", search)
	http.HandleFunc("/api/latest", latest)
	http.HandleFunc("/api/hotspot", hotspot)
}

func search(w http.ResponseWriter, r *http.Request) {
	text := r.FormValue("v")
	pagev := r.FormValue("p")
	var page int64
	var err error
	if pagev == "" {
		page = 1
	} else {
		page, err = strconv.ParseInt(pagev, 0, 0)
		if err != nil {
			page = 1
		}
	}
	text = strings.Replace(text, ",", " ", -1)
	text = strings.Replace(text, "，", " ", -1)
	words := strings.Split(text, " ")
	var wordres []bson.RegEx
	for _, word := range words {
	  if word!="" {
			  var re bson.RegEx
			  re.Pattern = word 
			  wordres = append(wordres, re)
		}
	}
	var result []NewsInfo
	var count int
	if len(wordres) > 0 {
		c := db.C("news")
		c.Find(bson.M{"title": bson.M{"$all": wordres}}).Select(bson.M{"tags": 0}).Sort(bson.D{{"pubdate", -1}, {"count", -1}}).Skip((int(page) - 1) * 10).Limit(10).All(&result)
		count, _ = c.Find(bson.M{"title": bson.M{"$all": wordres}}).Count()
	}
	js, err := json.Marshal(bson.M{"newslist": result, "count": count})
	handleError(err)
	w.Write(js)
}

func latest(w http.ResponseWriter, r *http.Request) {
	c := db.C("news")
	var result []NewsInfo
	c.Find(nil).Select(bson.M{"tags": 0}).Sort(bson.D{{"pubdate", -1}, {"count", -1}}).Limit(50).All(&result)
	js, err := json.Marshal(bson.M{"newslist": result})
	handleError(err)
	w.Write(js)
}

func hotspot(w http.ResponseWriter, r *http.Request) {
	c := db.C("news")
	var result []NewsInfo
	c.Find(nil).Select(bson.M{"tags": 0}).Sort(bson.D{{"count", -1}, {"pubdate", -1}}).Limit(50).All(&result)
	js, err := json.Marshal(bson.M{"newslist": result})
	handleError(err)
	w.Write(js)
}
