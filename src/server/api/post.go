package main

import (
	"encoding/json"
	"fmt"
	"launchpad.net/mgo/bson"
	"log"
	"net/http"
)

var _ = fmt.Println
var _ = log.Fatal

func init() {
	http.HandleFunc("/api/post", post)
}

func post(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		t := r.FormValue("type")
		ver := r.FormValue("version")
		if t == "news" {
			if ver == "1.0" {
				data := r.FormValue("data")
				var newsList []NewsInfo
				err := json.Unmarshal([]byte(data), &newsList)
				if err != nil {
					http.Error(w, "Data Error", http.StatusForbidden)
					return
				}
				c := db.C("news")
				for _, v := range newsList {
					c.Upsert(bson.M{"url": v.Url}, bson.M{"$set": v})
				}
				fmt.Fprintf(w, "OK")
				return
			} else {
				http.Error(w, "Version Not Allowed", http.StatusForbidden)
				return
			}
		} else {
			http.Error(w, "Type Not Allowed", http.StatusForbidden)
			return
		}
	} else {
		http.Error(w, "", http.StatusMethodNotAllowed)
		return
	}
}
