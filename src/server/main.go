package main

import (
	"encoding/xml"
	"errors"
	"fmt"
	"launchpad.net/mgo"
	_ "launchpad.net/mgo/bson"
	"net/http"
	"os"
)

var session *mgo.Session
var db *mgo.Database

type Config struct {
	DBHost string `xml:"mongodb>host"`
	DBUser string `xml:"mongodb>username"`
	DBPass string `xml:"mongodb>password"`
	DBName string `xml:"mongodb>dbname"`
}

func init() {
	f, err := os.OpenFile("config.xml", os.O_RDONLY, 0600)
	handleError(err)
	defer f.Close()
	d := xml.NewDecoder(f)
	var config Config
	err = d.Decode(&config)
	handleError(err)
	session, err = mgo.Dial(config.DBHost)
	handleError(err)
	if config.DBName == "" {
		handleError(errors.New("database name cannot be empty"))
	}
	db = session.DB(config.DBName)
	// session.SetSafe(&mgo.Safe{})
	if config.DBUser != "" {
		err := db.Login(config.DBUser, config.DBPass)
		handleError(err)
	}
}

func test(w http.ResponseWriter, r *http.Request) {
	c := db.C("news")
	var newsList []NewsInfo
	err := c.Find(nil).All(&newsList)
	handleError(err)
	fmt.Fprintln(w, newsList)
}

func main() {
	http.Handle("/static/", http.FileServer(http.Dir("")))
	http.HandleFunc("/test", test)
	handleError(http.ListenAndServe(":8080", nil))
	session.Close()
}
