package main

import (
	"fmt"
	"html/template"
	"net/http"
)

var _ = fmt.Println
var indextmpl *template.Template

func init() {
	var err error
	indextmpl, err = template.ParseFiles("templates/index.html")
	handleError(err)
	http.HandleFunc("/", index)
}

func index(w http.ResponseWriter, r *http.Request) {
	//indextmpl, err := template.ParseFiles("templates/index.html")	
	//if err != nil {
	//	log.Fatal(err)
	//}
	indextmpl.Execute(w, nil)
}
