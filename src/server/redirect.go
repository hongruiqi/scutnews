package main

import (
	"fmt"
	"launchpad.net/mgo/bson"
	"log"
	"net/http"
)

var _ = fmt.Println
var _ = log.Fatal

func init() {
	http.HandleFunc("/redirect", redirect)
}

func redirect(w http.ResponseWriter, r *http.Request) {
	url := r.FormValue("url")
	c := db.C("news")
	c.Update(bson.M{"url": url}, bson.M{"$inc": bson.M{"count": 1}})
	http.Redirect(w, r, url, http.StatusFound)
}
