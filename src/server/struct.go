package main

import ()

type NewsInfo struct {
	Title   string
	Url     string
	Cata    string
	From    string
	Tags    []string `json:"-"`
	PubDate string
	Count   int `bson:",omitempty"`
}
